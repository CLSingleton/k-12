![e1337 Training Logo](logo.png)

 This information is used for sharing resources with K-12 and should be used for Students, Teachers, and Parents: 

 1. To give Students information on pathways, resources, and fun ways to get started
 2. To give Teachers resources that can easily be tied to state standards (ie. State of Georgia Department of Education)
 3. To give Parents information on paythways, resources, and oppotunities if they are starting a career in Information Security



There should be useful info here for a full age range of participant and if you are looking to achieve certifications IT, Cyber Security, etc. please reach out to us!

Contact Details: e1337training@augusta.edu



# Getting started
The Link below should be used to view the webpage. 

Material Page link: https://gitlab.com/e1337training/k-12/k-12/-/blob/main/material_links.md

WebLink Here: https://e1337training.gitlab.io/k-12/k-12
