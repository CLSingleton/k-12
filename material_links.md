![image](logo.png)

<br>

# Table of Contents

   
 [K - 5](#k---5)  
 [6 - 8](#6---8)  
 [9 - 12](#9---12)  
 [Online Saftey](#online-saftey)  
 [Teachers](#teachers)  
 [Parents](#Parents)  
 [Community](#Community)


<br>
<br>
<br>
<!--  This section is for all K - 12 Students -->

# <u>Notice</u> 
Some of the materials are listed twice because they fit in multiple categories. These materials are updated as new resources are learned and made available. 

# K - 5

Learn the secrets of cyber security with our K-5 resources! Learn how to protect your personal information, stay safe online, and defend against cyber threats. From interactive activities to engaging lessons, our resources make learning about cyber security fun and easy. Join the cyber security revolution and safeguard your digital world today and follow along with this section!

1. [SavvyCyberKids](https://savvycyberkids.org/)  
   Since 2007, Savvy Cyber Kids has been helping parents and teachers educate children in cyber safety, cyber ethics and other aspects of their daily tech lives. Savvy Cyber Kids offers a platform of FREE educational resources for preschool through high school students in two tracks: one for families, and one for educators.
2. 

# 6 - 8

1. Content Here

# 9 - 12

### CyberStart America:
CyberStart America is the most enjoyable way to discover your talent, advance your skills and win scholarships in cybersecurity!
Get free access to CyberStart, an immersive cybersecurity training game for high school students, with over 200 fun-to-play challenges. Perfect for beginners!
<p>No support for embedded PDFs. Please download the PDF to view it: 

<a href="https://gitlab.com/e1337training/k-12/6-8/-/raw/main/Assets/CyberStart-Student%20Poster%202022%20REV%20Final%20Print.pdf?inline=false">Download PDF</a></p>

https://www.cyberstartamerica.org/



# Teachers

1. [SavvyCyberKids](https://savvycyberkids.org/)  
   Since 2007, Savvy Cyber Kids has been helping parents and teachers educate children in cyber safety, cyber ethics and other aspects of their daily tech lives. Savvy Cyber Kids offers a platform of FREE educational resources for preschool through high school students in two tracks: one for families, and one for educators.
2. 

# Parents


# Community


# Online Saftey

1. [SavvyCyberKids](https://savvycyberkids.org/)  
Since 2007, Savvy Cyber Kids has been helping parents and teachers educate children in cyber safety, cyber ethics and other aspects of their daily tech lives. Savvy Cyber Kids offers a platform of FREE educational resources for preschool through high school students in two tracks: one for families, and one for educators.


[FBI - Safe Online Surfing](https://sos.fbi.gov/)

[Missing kids](https://www.missingkids.org/netsmartz/home)

[National Cybersecurity Alliance](https://staysafeonline.org/resources/)

[FCC Smartphone Security Checker](https://www.fcc.gov/smartphone-security)


# General Cyber Pathway Resource

## CyberStart America:
CyberStart America is the most enjoyable way to discover your talent, advance your skills and win scholarships in cybersecurity!
Get free access to CyberStart, an immersive cybersecurity training game for high school students, with over 200 fun-to-play challenges. Perfect for beginners!
<p>No support for embedded PDFs. Please download the PDF to view it: 

<a href="https://gitlab.com/e1337training/k-12/k-12/-/blob/main/assets/CyberStart-Student%20Poster%202022%20REV%20Final%20Print%20(2).pdf?inline=false">Download PDF</a></p>

https://www.cyberstartamerica.org/

----
## CyberPatriot:
https://www.uscyberpatriot.org/home

## Public Broadcasting Service (PBS): 
Looking for fun quizzes, games, and a library of resources to educate children, parents, and teachers about cybersecurity? PBS has what you need! The resources accommodate multiple age groups. Visit:

https://www.pbs.org/wgbh/nova/labs/lab/cyber/and

PBS CyberChase: https://pbskids.org/cyberchase/

----
## Code.org 
has a wide range of lessons, how-to guides, videos and more!
For Ages: Kindergarten – Grade 12
Topic: programming
https://code.org/ 

## Unplugged:
A collection of free learning activities that teach Computer Science through engaging games and puzzles that use cards, string, crayons, and lots of running around.
For Ages: Kindergarten – Grade 12
Topic: programming, networking, and security
http://csunplugged.org/

## Khan Academy:
Offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom.
For Ages: Kindergarten – Grade 12
Topic: programming
https://www.khanacademy.org/

<br>
<br>
## Links for parents, teachers, and students to learn coding, cybersecurity concepts, and more! This website has a plethora of resources but I noticed that some of them I have already added on this page so you can skip them. 
https://www.cybher.org/resources/

Scratch:
you can program your own interactive stories, games, and animations — and share your creations with others in the online community.
For Ages: Kindergarten – Grade 12
Topic: programming
https://scratch.mit.edu/



CyberDegrees:
has a wide variety of resources for post-secondary graduates who are interested in pursuing a degree in Cybersecurity!
For Ages: Post-secondary graduates
Topic: cyber security education and resources
CyberDegrees.org



Skills for All:
https://skillsforall.com/


Try hack me:
https://tryhackme.com/

Cyberseek.org:
https://www.cyberseek.org/

